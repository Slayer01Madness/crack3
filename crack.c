#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

struct entry 
{
    // FILL THIS IN
    char * pass;
    char * hash;
};


int comp(const void *a, const void *b)
{
    return strcmp((*(struct entry*)a).hash, (*(struct entry*)b).hash);
}

int bcompare(const void *t, const void *a)
{
    int compar = strcmp((char *)t, (*(struct entry*)a).hash);
    if(compar == 0)
    {
        printf("Found %s it was %s!\n",(char *)t, (*(struct entry*)a).pass);
        return compar;
    }
    else
    {
        return compar;
    }
}

int file_length(char *filename)
{
    struct stat fileinfo;
    if (stat(filename, &fileinfo) == -1)
    {
        return -1;
    }
    else
    {
        return fileinfo.st_size;
    }
}

// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    
    int len = file_length(filename);
    if (len == -1)
    {
        printf("Couldn't get length of file %s\n", filename);
        exit(1);
    }
    
    char *file_contents = malloc(len);
    
    FILE *fp = fopen(filename, "r");
    if(!fp)
    {
        printf("Coundn't open %s for reading\n", filename);
        exit(1);
    }
    fread(file_contents, 1, len, fp);
    fclose(fp);
    
    int line_count = 0;
    for (int i = 0; i < len; i++)
    {
        if (file_contents[i] == '\n')
        {
            file_contents[i] = '\0';
            line_count++;
        }
    }
    
    struct entry *pass_hash;
    pass_hash = malloc(line_count * sizeof(struct entry));
    
    int size_dlen = 0;
    int c = 0;
    for (int i = 0; i < line_count; i++)
    {
        pass_hash[i].pass = &file_contents[c];
        pass_hash[i].hash = md5(&file_contents[c], strlen(&file_contents[c]));
        
        while (file_contents[c] != '\0')
        {
            
            c++;
        }
        
        c++;
        size_dlen ++;
    }
    
    *size = size_dlen;
    return pass_hash;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    printf("Making the struct\n");
    int dlen = 0;
    struct entry *dict = read_dictionary(argv[2], &dlen);
    
    char ** a = dict[0].pass;
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    printf("Sorting\n");
    qsort(dict, dlen, sizeof(struct entry), comp);
    
    

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    FILE *fp;
    fp = fopen(argv[1], "r");
    if (!fp)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    printf("Comparing\n");
    char hash[HASH_LEN + 1];
    while(fgets(hash, HASH_LEN +1, fp) != NULL)
    {
        hash[strlen(hash)-1] = '\0';
        
        bsearch(hash, dict, dlen, sizeof(struct entry), bcompare);
    }
    
    fclose(fp);
    for(int i = 0; i < dlen; i++)
    {
        free(dict[i].hash);
    }
    free(a);
    free(dict);
}
